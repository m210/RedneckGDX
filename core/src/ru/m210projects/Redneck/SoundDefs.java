//Copyright (C) 1996, 2003 - 3D Realms Entertainment
//
//This file is part of Duke Nukem 3D version 1.5 - Atomic Edition
//
//Duke Nukem 3D is free software; you can redistribute it and/or
//modify it under the terms of the GNU General Public License
//as published by the Free Software Foundation; either version 2
//of the License, or (at your option) any later version.
//
//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
//
//See the GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with this program; if not, write to the Free Software
//Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
//
//Original Source: 1996 - Todd Replogle
//Prepared for public release: 03/21/2003 - Charlie Wiederhold, 3D Realms
//This file has been modified by Alexander Makarov-[M210] (m210-2007@mail.ru)

package ru.m210projects.Redneck;

public class SoundDefs {

    public static final int KICK_HIT = 0;
    public static final int PISTOL_RICOCHET = 1;
    public static final int PISTOL_BODYHIT = 2;
    public static final int PISTOL_FIRE = 3;
    public static final int EJECT_CLIP = 4;
    public static final int INSERT_CLIP = 5;
    public static final int CHAINGUN_FIRE = 6;
    public static final int RPG_SHOOT = 7;
    public static final int POOLBALLHIT = 8;
    public static final int RPG_EXPLODE = 9;
    public static final int SHRINKER_FIRE = 11;
    public static final int PIPEBOMB_BOUNCE = 13;
    public static final int PIPEBOMB_EXPLODE = 14;
    public static final int LASERTRIP_EXPLODE = 17;
    public static final int VENT_BUST = 18;
    public static final int GLASS_BREAKING = 19;
    public static final int GLASS_HEAVYBREAK = 20;
    public static final int SHORT_CIRCUIT = 21;
    public static final int ITEM_SPLASH = 22;
    public static final int DUKE_GASP = 25;
    public static final int DUKE_CRACK = 33;
    public static final int DUKE_DRINKING = 36;
    public static final int DUKE_ONWATER = 40;
    public static final int DUKE_LAND = 42;
    public static final int DUKE_UNDERWATER = 48;
    public static final int DUKE_JETPACK_IDLE = 50;
    public static final int SQUISHED = 69;
    public static final int TELEPORTER = 70;
    public static final int ELEVATOR_ON = 71;
    public static final int ELEVATOR_OFF = 73;
    public static final int SUBWAY = 75;
    public static final int SWITCH_ON = 76;
    public static final int EARTHQUAKE = 81;
    public static final int END_OF_LEVEL_WARN = 83;
    public static final int SOMETHING_DRIPPING = 92;
    public static final int SHOTGUN_FIRE = 109;
    public static final int THUD = 158;
    public static final int SHOTGUN_COCK = 169;
    public static final int GENERIC_AMBIENCE17 = 177;
    public static final int BONUS_SPEECH1 = 195;
    public static final int BONUS_SPEECH2 = 196;
    public static final int BONUS_SPEECH3 = 197;
    public static final int BONUS_SPEECH4 = 199;
    public static final int DUKE_LAND_HURT = 200;
    public static final int DUKE_SEARCH2 = 207;
    public static final int DUKE_CRACK2 = 208;
    public static final int DUKE_SEARCH = 209;
    public static final int DUKE_GET = 210;
    public static final int DUKE_LONGTERM_PAIN = 211;
    public static final int DUKE_USEMEDKIT = 216;
    public static final int DUKE_TAKEPILLS = 217;
    public static final int DUKE_GOTHEALTHATLOW = 229;
    public static final int EXITMENUSOUND = 243;
    public static final int DUKE_SCREAM = 245;
    public static final int RATTY = 247;
    public static final int ALIEN_SWITCH1 = 272;
    public static final int WITNESSSTAND = 330;
    // MAXIMUM NUMBER OF SOUNDS: 450 ( 0-449 )
}
